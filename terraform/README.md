# Basin
These modules are responsible for building the Basin project and all it's
environments. These modules and scripts require the
"Organization Administrator" GCP role.

See,
* https://cloud.google.com/resource-manager/docs/creating-managing-organization
* https://cloud.google.com/iam/docs/understanding-roles

### How-to use
Follow these steps,

1. Authenticate the GCloud SDK with a GCP Account - so that local invocations
    of GCloud will be able to configure resources within the GCP Account.
```sh
  gcloud auth login --no-launch-browser
  <Copy URL to browser and authenticate Google SDK access to your GCP Account>
  <Enter verification code from browser into Command Line>
```
2. Determine the Organization Resource ID.
```sh
  gcloud organizations list
```
3. Determine the Billing Account ID.
```sh
  gcloud beta billing accounts list
```
4. Run the init script. This will configure the GCP account with the
  administrative means to Terraform this Project's infrastructure. 
```sh
  cd ${PROJECT_DIR}/terraform/
  bin/initGcp.sh "${GCP_ORGANIZATION_ID}" "${GCP_BILLING_ACCOUNT_ID}"
```
5. Terraform this Project's infrastructure.
```sh
  export TF_VAR_org_id="${GCP_ORGANIZATION_ID}"
  export TF_VAR_billing_account="${GCP_BILLING_ACCOUNT}"
  export TF_VAR_build_id="1"
  export GOOGLE_APPLICATION_CREDENTIALS="${GCP_CRED_PATH}"
  terraform init
  terraform plan -out="./out.tfstate"
  terraform apply out.tfstate
```
