# VARIABLES -------------------------------------------------------------------
variable "org_id" {}
variable "billing_account" {}                    
variable "project_folder_id" {}
variable "project_name" {}
variable "project_id" {}                       
variable "region"       { default = "us-central1" }                             
variable "zone"         { default = "us-central1-c" }                               

variable "docker_config_json" {}
variable "img_scheduled_db"  {}
variable "img_scheduled_api" {}

# PROJECT CONFIG --------------------------------------------------------------
provider "google" {
  region  = "${var.region}"
  zone    = "${var.zone}"
}

#resource "google_folder" "environment_root" {               
#  display_name = "${var.project_folder_name}"                         
#  parent       = "organizations/${var.org_id}"         
#}                                                 
                                                 
module "project" {
  source = "./modules/project/"

  org_id          = "${var.org_id}"
  billing_account = "${var.billing_account}"
  project_name    = "${var.project_name}"
  project_id      = "${var.project_id}"
  folder_id       = "${var.project_folder_id}"

  docker_config_json = "${var.docker_config_json}"
  img_scheduled_db  = "${var.img_scheduled_db}"
  img_scheduled_api = "${var.img_scheduled_api}"
}

# OUTPUTS ---------------------------------------------------------------------
output "client_certificate" {
  value = "${module.project.client_certificate}"
}
output "client_key" {
  value = "${module.project.client_key}"
}

output "cluster_ca_certificate" {
  value = "${module.project.cluster_ca_certificate}"
}

output "host" {
  value = "${module.project.host}"
}

output "username" {
  value = "${module.project.username}"
}

output "password" {
  value = "${module.project.password}"
}
output "lb_ip" {
  value = "${module.project.lb_ip}"
}

