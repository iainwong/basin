package com.basin.gtfs.domainmodel;

import java.util.Date;

import javax.persistence.MappedSuperclass;
import javax.persistence.EntityListeners;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
  value = {"createdAt", "updatedAt"},
  allowGetters = true
)
public abstract class AuditModel implements Serializable {
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "created_at", nullable = false, updatable = false)
  @CreatedDate
  private Date createdAt;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "updated_at", nullable = false)
  @LastModifiedDate
  private Date updatedAt;

  /* GETTERS & SETTERS */
  public Date getCreatedAt()        { return createdAt; }
  public void setCreatedAt(Date d)  { createdAt = d; }
  public Date getUpdatedAt()        { return updatedAt; }
  public void setUpdatedAt(Date d)  { updatedAt = d; }
}
