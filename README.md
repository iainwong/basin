# Basin

Basin is a collection of General Transit Feed Spec (GTFS) Data published by transit agencies, accessible by restful and graphql interfaces. It provides networked access for consumption and use by Applications, in an interoperable way.

## Table of Contents,
- [README](terraform/README.md)

### Building on GCP w- Terraform and GitLab
Configure GitLab's runner environment variables,
(Project -> Settings -> CI/CD -> Variables -> Expand),
  1.  Create the `TF_VAR_org_id` variable with the value of
      GCP's Organization ID,

      ```sh
        gcloud organizations list
      ```

  2.  Create the `TF_VAR_billing_account` variable with the value of
      the Billing Account ID associated with the previous Organization ID,

      ```sh
        gcloud beta billing accounts list
      ```

  3.  Create the `BASIN_SERVICE_ACCOUNT` variable with the value of your
      administrative GCP service account. The
      `${PROJECT_DIR}/terraform/bin/initGcp.sh` is responsible for creating this
      Service Account and populating the following JSON Key file. Please read
      `${PROJECT_DIR}/terraform/README.md` for description of the `initGcp.sh`
      script.

      On MacOS,
      ```sh
        cat ~/.config/gcloud/iainwong-terraform-admin-3.json | gbase64 -w0
      ```
      On Linux,
      ```sh
        cat ~/.config/gcloud/iainwong-terraform-admin-3.json | base64 -w0
      ```

### Building Locally w- Docker Compose
Build,
1. `$PROJECT_DIR/bin/build.local.sh`

Run,
1. `docker-compose up -d`

### Building Locally w- Docker
Compile byte-code,
```sh
1. $PROJECT_DIR/bin/build.local.sh
```

Source environment variables,
```sh
1. export GOOGLE_CLOUD_KEYFILE_JSON="/Users/iainwong/.creds/basin-f0f64f051fc0.json" \
    && export GOOGLE_APPLICATION_CREDENTIALS="/Users/iainwong/.creds/basin-f0f64f051fc0.json"
    && export GOOGLE_PROJECT=basin-220815 \
    && export SPRING_PROFILES_ACTIVE=PRODUCTION \
    && export BZN_NETWORK=bzn_net \
    && export BZD_PSQL_HOSTNAME=bzn_scheduled_db \
    && export BZD_PSQL_PORT=5432 \
    && export BZD_PSQL_DB=gtfs_scheduled \
    && export BZD_PSQL_USER=testuser \
    && export BZD_PSQL_PASS=testpass \
    && export BZA_HOSTNAME=bzn_scheduled_api
```

Create network,
```sh
1. cd $PROJECT_DIR
2. docker network create ${BZN_NETWORK} 
```

Run db,
```sh
1. cd $PROJECT_DIR
2. docker stop ${BZD_PSQL_HOSTNAME}
3. docker rm ${BZD_PSQL_HOSTNAME}
4. docker build -t basin_scheduled_db -f ./gtfs-scheduled-api/Dockerfile-gtfs-scheduled-db ./gtfs-scheduled-api/
5. docker run -d -it --name=${BZD_PSQL_HOSTNAME} \
      -e POSTGRES_DB=${BZD_PSQL_DB}\
      -e POSTGRES_USER=${BZD_PSQL_USER} \
      -e POSTGRES_PASSWORD=${BZD_PSQL_PASS} \
      --network ${BZN_NETWORK} basin_scheduled_db
```

Run api,
```sh
1. cd $PROJECT_DIR
?. docker stop ${BZA_HOSTNAME}
?. docker rm ${BZA_HOSTNAME}
2. docker build -t basin_scheduled_api -f ./gtfs-scheduled-api/Dockerfile-gtfs-scheduled-api ./gtfs-scheduled-api/
3. docker run -d -it -p 8080:8080 --name=${BZA_HOSTNAME} \
      -e SPRING_PROFILES_ACTIVE=${SPRING_PROFILES_ACTIVE} \
      -e BZD_PSQL_HOSTNAME=${BZD_PSQL_HOSTNAME} \
      -e BZD_PSQL_PORT=${BZD_PSQL_PORT} \
      -e BZD_PSQL_DB=${BZD_PSQL_DB} \
      -e BZD_PSQL_USER=${BZD_PSQL_USER} \
      -e BZD_PSQL_PASS=${BZD_PSQL_PASS} \
      --network ${BZN_NETWORK} basin_scheduled_api
```

### Building in CI w- Gitlab

