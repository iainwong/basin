# gtfs-schedule-api

### How-to run with Gradle
```sh
$ cd ./gtfs-scheduled-api/
$ ./gradlew bootRun
```

### How-to run with Docker
```sh
$ docker run --rm -v "$PWD":/home/gradle/project -w /home/gradle/project -p 8080:8080 gradle gradle bootRun
```
