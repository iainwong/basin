package com.basin.gtfs;

import com.basin.gtfs.domainmodel.Route;
import com.basin.gtfs.persistencemodel.RouteRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class Application {

  private static final Logger log =
    LoggerFactory.getLogger(Application.class);

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Bean
  public CommandLineRunner demo(RouteRepository repo) {
    return (args) -> {

      /* Create & Persist Demo Objects */
      repo.save(new Route("feedID1", "routeID1", "agencyID1"));
      repo.save(new Route("feedID2", "routeID2", "agencyID2"));
      repo.save(new Route("feedID3", "routeID3", "agencyID3"));

      log.info("RouteRepository.findAll():");
      log.info("#");
      for (Route r : repo.findAll()) {
        log.info(r.toString());
      }

      log.info("RouteRepository.findByRouteID():");
      log.info("#");
      //repo.findByRouteID("blank", "blank").ifPresent(log::info);

      log.info("Done.");
    };
  }
}

