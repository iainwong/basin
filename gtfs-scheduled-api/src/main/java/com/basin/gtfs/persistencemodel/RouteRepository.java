package com.basin.gtfs.persistencemodel;

import com.basin.gtfs.domainmodel.Route;

import java.util.List;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RouteRepository extends JpaRepository<Route, Long> {
  //List<Route> findByRouteID(String routeID);
  //List<Route> findByRouteID(String feedID, String routeID);
}
