package com.basin.gtfs.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


import com.basin.gtfs.domainmodel.Route;
import com.basin.gtfs.model.RouteDTO;
import com.basin.gtfs.persistencemodel.RouteRepository;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

import org.modelmapper.ModelMapper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageImpl;

import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class GetRouteController {

  @Autowired
  private RouteRepository routeRepository;

  @GetMapping("/routes")
  public Page<RouteDTO> getRoutes(Pageable pageable) {
    ModelMapper modelMapper = new ModelMapper();

    Page<Route> rPage = routeRepository.findAll(pageable);
    List<Route> rList = new ArrayList<Route>();

    if (rPage.hasContent())
      rList = rPage.getContent();

    List<RouteDTO> dtoList = rList.stream()
      .map(route -> modelMapper.map(route, RouteDTO.class))
      .collect(Collectors.toList());

    return new PageImpl<>(dtoList);
  }

  @PostMapping("/routes")
  public RouteDTO createRoute(@Valid @RequestBody RouteDTO dto) {
    ModelMapper modelMapper = new ModelMapper();

    Route route = modelMapper.map(dto, Route.class);
    route = routeRepository.save(route);

    dto = modelMapper.map(route, RouteDTO.class);

    return dto;
  }

  @PutMapping("/routes/{routeID}")
  public RouteDTO updateRoute(
    @PathVariable Long routeID,
    @Valid @RequestBody RouteDTO dto) throws Exception {
    ModelMapper modelMapper = new ModelMapper();

    Route route = routeRepository.findById(routeID)
      .map(routeLambda -> {
        routeLambda.setFeedID(dto.getFeedID());
        routeLambda.setAgencyID(dto.getAgencyID());
        routeLambda.setRouteShortName(dto.getRouteShortName());
        return routeRepository.save(routeLambda);
      }).orElseThrow(() -> new Exception("Route not found with ID " + routeID));

    return modelMapper.map(route, RouteDTO.class);
  }
}
