terraform {                                           
 backend "gcs" {                                      
   bucket  = "iainwong-terraform-admin-1"   
   prefix  = "terraform/state"                        
   project = "iainwong-terraform-admin-1"   
 }                                                    
}                                                     
