# gtfs-loader

To run,
```sh
$ docker build -t basin .
$ docker run --name pserver -e POSTGRES_USER=puser -e POSTGRES_PASSWORD=mysecretpassword -d postgres
$ docker run --name test1 --link postgres basin
```
