package com.basin.gtfs.domainmodel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.util.Objects;

@Entity
@Table(name = "route")
public class Route extends AuditModel {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;
  private String feedID;
  private String routeID;
  private String agencyID;
  private String routeShortName;
  private String routeLongName;
  private String routeDesc;
  private int    routeType;
  private String routeUrl;
  private String routeColor;
  private String routeTextColor;
  private String routeSortOrder;

  protected Route() {}
  public Route(String feedID, String routeID, String agencyID) {
    Objects.requireNonNull(feedID, "feedID can not be null.");
    Objects.requireNonNull(routeID, "routeID can not be null.");
    Objects.requireNonNull(agencyID, "agencyID can not be null.");
    this.feedID = feedID;
    this.routeID = routeID;
    this.agencyID = agencyID;
  }

  /* OBJECT BEHAVIOUR CONTRACTS */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Route {\n");
    sb.append("  feedID: ").append(toIndentedString(feedID)).append("\n");
    sb.append("  routeID: ").append(toIndentedString(routeID)).append("\n");
    sb.append("  agencyID: ").append(toIndentedString(agencyID)).append("\n");
    sb.append("  routeShortName: ").append(toIndentedString(routeShortName)).append("\n");
    sb.append("  routeLongName: ").append(toIndentedString(routeLongName)).append("\n");
    sb.append("  routeDesc: ").append(toIndentedString(routeDesc)).append("\n");
    sb.append("  routeType: ").append(toIndentedString(routeType)).append("\n");
    sb.append("  routeUrl: ").append(toIndentedString(routeUrl)).append("\n");
    sb.append("  routeColor: ").append(toIndentedString(routeColor)).append("\n");
    sb.append("  routeTextColor: ").append(toIndentedString(routeTextColor)).append("\n");
    sb.append("  routeSortOrder: ").append(toIndentedString(routeSortOrder)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /* UTIL */
  private String toIndentedString(java.lang.Object o) {
    String indented;

    if (null == o)
      indented = "null";
    else
      indented = o.toString().replace("\n", "\n  ");

    return indented;
  }

  /* GETTERS & SETTERS */
  public String getFeedID()         { return feedID; }
  public String getRouteID()        { return routeID; }
  public String getAgencyID()       { return agencyID; }
  public String getRouteShortName() { return routeShortName; }
  public String getRouteLongName()  { return routeLongName; }
  public String getRouteDesc()      { return routeDesc; }
  public int    getRouteType()      { return routeType; }
  public String getRouteUrl()       { return routeUrl; }
  public String getRouteColor()     { return routeColor; }
  public String getRouteTextColor() { return routeTextColor; }
  public String getRouteSortOrder() { return routeSortOrder; }

  public void setFeedID (String s)         { feedID = s; }
  public void setRouteID (String s)        { routeID = s; }
  public void setAgencyID (String s)       { agencyID = s; }
  public void setRouteShortName (String s) { routeShortName = s; }
  public void setRouteLongName (String s)  { routeLongName = s; }
  public void setRouteDesc (String s)      { routeDesc = s; }
  public void setRouteType (int i)         { routeType = i; }
  public void setRouteUrl (String s)       { routeUrl = s; }
  public void setRouteColor (String s)     { routeColor = s; }
  public void setRouteTextColor (String s) { routeTextColor = s; }
  public void setRouteSortOrder (String s) { routeSortOrder = s; }
}
