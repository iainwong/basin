# VARIABLES -------------------------------------------------------------------
variable "org_id" {}
variable "billing_account" {}
variable "project_name" {}
variable "project_id" {}
variable "folder_id"    {}

variable "docker_config_json" {}
variable "img_scheduled_db"  {}
variable "img_scheduled_api" {}

# MAIN ------------------------------------------------------------------------
resource "google_project" "basin" {            
  name            = "${var.project_name}"
  project_id      = "${var.project_id}" 
  billing_account = "${var.billing_account}"     
  folder_id       = "${var.folder_id}"
}                                                

resource "google_project_services" "basin" {
  project = "${google_project.basin.project_id}"

  services   = [
    "containerregistry.googleapis.com",
    "pubsub.googleapis.com",
    "storage-component.googleapis.com",
    "oslogin.googleapis.com",
    "bigquery-json.googleapis.com",
    "clouddebugger.googleapis.com",
    "serviceusage.googleapis.com",
    "servicemanagement.googleapis.com",
    "cloudtrace.googleapis.com",
    "monitoring.googleapis.com",
    "logging.googleapis.com",
    "storage-api.googleapis.com",
    "sql-component.googleapis.com",
    "datastore.googleapis.com",
    "iam.googleapis.com",
    "iamcredentials.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "cloudbilling.googleapis.com",
    "compute.googleapis.com",
    "cloudapis.googleapis.com",
    "container.googleapis.com"
  ]
}

module "gtfs_scheduled_api" {
  source = "./gtfs-scheduled-api"
  
  project_name = "${google_project.basin.name}"
  #project_id   = "${google_project.basin.project_id}"
  project_id   = "${google_project_services.basin.project}"
  
  docker_config_json = "${var.docker_config_json}"
  img_scheduled_db  = "${var.img_scheduled_db}"
  img_scheduled_api = "${var.img_scheduled_api}"

  services = "${google_project_services.basin.services}"
}

# OUTPUTS ---------------------------------------------------------------------
output "client_certificate" {
  value = "${module.gtfs_scheduled_api.client_certificate}"
}
output "client_key" {
  value = "${module.gtfs_scheduled_api.client_key}"
}

output "cluster_ca_certificate" {
  value = "${module.gtfs_scheduled_api.cluster_ca_certificate}"
}

output "host" {
  value = "${module.gtfs_scheduled_api.host}"
}
output "username" {
  value = "${module.gtfs_scheduled_api.username}"
}

output "password" {
  value = "${module.gtfs_scheduled_api.password}"
}

output "lb_ip" {
  value = "${module.gtfs_scheduled_api.lb_ip}"
}

