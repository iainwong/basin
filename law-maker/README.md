# law-maker
This project constructs the necessary bootstrap resources to erect other
terraform projects.

Primarily this project's responsibilities are,
1. GCP Admin Project (of the form "${USER}-terraform-admin")
2. GCP User (of the form "${USER}-terraform-admin")
3. GCP User Roles
4. Enabling GCP services

### How-to use
Obtain your organization id - from the ID column,
```sh
  gcloud organizations list
```

Set the `TF_VAR_org_id` variable in the file `bin/tfVars.sh`,
```sh
  export TF_VAR_org_id=477817496373 // the org id
```

Obtain your billing account id,
```sh
  gcloud beta billing accounts list
```

Set the `TF_VAR_billing_account` variable in the file `bin/tfVars.sh`,
```sh
  export TF_VAR_org_id=477817496373 // ACCOUNT_ID
```

Create the necessary projects,
```sh
  1. cd ${PROJECT_DIR}/bin/
  2. source tfVars.sh
  3. source gcpVars.sh
  4. ./gcpInit.sh
```
