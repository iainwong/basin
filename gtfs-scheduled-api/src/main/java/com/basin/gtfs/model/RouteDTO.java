package com.basin.gtfs.model;

import java.util.Objects;

public class RouteDTO {
  private String feedID;
  private String routeID;
  private String agencyID;
  private String routeShortName;
  private String routeLongName;
  private String routeDesc;
  private int    routeType;
  private String routeUrl;
  private String routeColor;
  private String routeTextColor;
  private String routeSortOrder;

  public RouteDTO() {}
  public RouteDTO(String feedID, String routeID) {
    this();
    this.feedID = feedID;
    this.routeID = routeID;
  }

  /* OBJECT BEHAVIOUR CONTRACTS */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o)
      return true;
    if (null == o || getClass() != o.getClass())
      return false;
    RouteDTO route = (RouteDTO)o;
    return Objects.equals(feedID, route.getFeedID())
      && Objects.equals(routeID, route.getRouteID())
      && Objects.equals(agencyID, route.getAgencyID())
      && Objects.equals(routeShortName, route.getRouteShortName())
      && Objects.equals(routeLongName, route.getRouteLongName())
      && Objects.equals(routeDesc, route.getRouteDesc())
      && Objects.equals(routeType, route.getRouteType())
      && Objects.equals(routeUrl, route.getRouteUrl())
      && Objects.equals(routeColor, route.getRouteColor())
      && Objects.equals(routeTextColor, route.getRouteTextColor())
      && Objects.equals(routeSortOrder, route.getRouteSortOrder());
  }
  @Override
  public int hashCode() {
    return Objects.hash(feedID,
      routeID,
      agencyID,
      routeShortName,
      routeLongName,
      routeDesc,
      routeType,
      routeUrl,
      routeColor,
      routeTextColor,
      routeSortOrder);
  }
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class RouteDTO {\n");
    sb.append("  feedID: ").append(toIndentedString(feedID)).append("\n");
    sb.append("  routeID: ").append(toIndentedString(routeID)).append("\n");
    sb.append("  agencyID: ").append(toIndentedString(agencyID)).append("\n");
    sb.append("  routeShortName: ").append(toIndentedString(routeShortName)).append("\n");
    sb.append("  routeLongName: ").append(toIndentedString(routeLongName)).append("\n");
    sb.append("  routeDesc: ").append(toIndentedString(routeDesc)).append("\n");
    sb.append("  routeType: ").append(toIndentedString(routeType)).append("\n");
    sb.append("  routeUrl: ").append(toIndentedString(routeUrl)).append("\n");
    sb.append("  routeColor: ").append(toIndentedString(routeColor)).append("\n");
    sb.append("  routeTextColor: ").append(toIndentedString(routeTextColor)).append("\n");
    sb.append("  routeSortOrder: ").append(toIndentedString(routeSortOrder)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /* UTIL */
  private String toIndentedString(java.lang.Object o) {
    String indented;

    if (null == o)
      indented = "null";
    else
      indented = o.toString().replace("\n", "\n  ");

    return indented;
  }

  /* GETTERS & SETTERS */
  public String getFeedID()         { return feedID; }
  public String getRouteID()        { return routeID; }
  public String getAgencyID()       { return agencyID; }
  public String getRouteShortName() { return routeShortName; }
  public String getRouteLongName()  { return routeLongName; }
  public String getRouteDesc()      { return routeDesc; }
  public int    getRouteType()      { return routeType; }
  public String getRouteUrl()       { return routeUrl; }
  public String getRouteColor()     { return routeColor; }
  public String getRouteTextColor() { return routeTextColor; }
  public String getRouteSortOrder() { return routeSortOrder; }

  public void setFeedID (String s)         { feedID = s; }
  public void setRouteID (String s)        { routeID = s; }
  public void setAgencyID (String s)       { agencyID = s; }
  public void setRouteShortName (String s) { routeShortName = s; }
  public void setRouteLongName (String s)  { routeLongName = s; }
  public void setRouteDesc (String s)      { routeDesc = s; }
  public void setRouteType (int i)         { routeType = i; }
  public void setRouteUrl (String s)       { routeUrl = s; }
  public void setRouteColor (String s)     { routeColor = s; }
  public void setRouteTextColor (String s) { routeTextColor = s; }
  public void setRouteSortOrder (String s) { routeSortOrder = s; }
}
