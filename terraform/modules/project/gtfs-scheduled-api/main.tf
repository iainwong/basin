# VARIABLES -------------------------------------------------------------------
variable "project_name" {}
variable "project_id" {}

variable "docker_config_json" {}
variable "img_scheduled_db"  {}
variable "img_scheduled_api" {}

variable "services" { default = [], type = "list"} 

# MAIN ------------------------------------------------------------------------
resource "google_container_cluster" "gtfs_scheduled_api" {
  name               = "cluster-${var.project_id}"
  description        = "Container cluster for ${var.project_name} (project_id: ${var.project_id})"
  project            = "${var.project_id}"

  initial_node_count = 3

 #lifecycle {
 #  ignore_changes = ["node_pool"]
 #}

 #node_pool {
 # initial_node_count = 6
 #  name = "default-pool"
 #}
  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/compute",
    ]
  }
  provisioner "local-exec" {
    command = "echo ${var.services[0]}"
  }
}

#resource "google_container_node_pool" "gtfs_scheduled_api" {
#  name       = "node-pool-${var.project_id}"
#  project    = "${var.project_id}"
#  cluster    = "${google_container_cluster.gtfs_scheduled_api.name}"
#  node_count = 1
#
#  node_config {
#    preemptible  = true
#    machine_type = "n1-standard-1"
#
#    oauth_scopes = [
#      "compute-rw",
#      "storage-ro",
#      "logging-write",
#      "monitoring",
#    ]
#  }
#}

module "kubernetes_gtfs_scheduled_api_deployment" {
  source = "./kubernetes"
  host = "${google_container_cluster.gtfs_scheduled_api.endpoint}"
  username = "${google_container_cluster.gtfs_scheduled_api.master_auth.0.username}"
  password = "${google_container_cluster.gtfs_scheduled_api.master_auth.0.password}"

  client_certificate = "${google_container_cluster.gtfs_scheduled_api.master_auth.0.client_certificate}"
  client_key = "${google_container_cluster.gtfs_scheduled_api.master_auth.0.client_key}"
  cluster_ca_certificate = "${google_container_cluster.gtfs_scheduled_api.master_auth.0.cluster_ca_certificate}"

  docker_config_json = "${var.docker_config_json}"
  img_scheduled_db  = "${var.img_scheduled_db}"
  img_scheduled_api = "${var.img_scheduled_api}"
}


# OUTPUTS ---------------------------------------------------------------------
# The following outputs allow authentication and connectivity to the GKE Cluster.
output "client_certificate" {
  value = "${google_container_cluster.gtfs_scheduled_api.master_auth.0.client_certificate}"
}

output "client_key" {
  value = "${google_container_cluster.gtfs_scheduled_api.master_auth.0.client_key}"
}

output "cluster_ca_certificate" {
  value = "${google_container_cluster.gtfs_scheduled_api.master_auth.0.cluster_ca_certificate}"
}
output "host" {
  value = "${google_container_cluster.gtfs_scheduled_api.endpoint}"
}

output "username" {
  value = "${google_container_cluster.gtfs_scheduled_api.master_auth.0.username}"
}
output "password" {
  value = "${google_container_cluster.gtfs_scheduled_api.master_auth.0.password}"
}
output "lb_ip" {
  value = "${module.kubernetes_gtfs_scheduled_api_deployment.lb_ip}"
}
