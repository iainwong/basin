terraform {
 backend "gcs" {
   bucket  = "iainwong-terraform-admin-3"
   prefix  = "terraform/state"
   project = "iainwong-terraform-admin-3"
 }
}
