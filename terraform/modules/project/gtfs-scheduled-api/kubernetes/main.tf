# Module to perform Kubernetes Deployments
# VARIABLES -------------------------------------------------------------------
variable "host"   {}
variable "username" {}
variable "password" {}
variable "client_certificate" {}
variable "client_key" {}
variable "cluster_ca_certificate" {}

variable "docker_config_json" {}
variable "img_scheduled_db"  {}
variable "img_scheduled_api" {}

# MAIN ------------------------------------------------------------------------
# Authenticate via TLS Certificate Credentials
provider "kubernetes" {
  host = "https://${var.host}"
  username = "${var.username}"
  password = "${var.password}"
  
  client_certificate     = "${base64decode(var.client_certificate)}"
  client_key             = "${base64decode(var.client_key)}"
  cluster_ca_certificate = "${base64decode(var.cluster_ca_certificate)}"
}

# Create Private Registry Secret
resource "kubernetes_secret" "private_docker_registry" {
  metadata {
    name = "docker-cfg"
  }

  data {
    ".dockerconfigjson" = "${base64decode(var.docker_config_json)}"
  }

  type = "kubernetes.io/dockerconfigjson"
}

# Basin Scheduled DB Deployment
resource "kubernetes_deployment" "bzn_scheduled_db_deployment" {
  metadata {
    name = "terraform-bzn-scheduled-db-deployment"
    labels {
      test = "bzn-scheduled-db"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels {
        test = "bzn-scheduled-db"
      }
    }

    template {
      metadata {
        labels {
          test = "bzn-scheduled-db"
        }
      }

      spec {
        image_pull_secrets {
          name = "docker-cfg" 
        }
        container {
          image = "${var.img_scheduled_db}"
          name  = "basin-scheduled-db"
          port {
            container_port = 5432
          }
          env   = [
            {
              name = "POSTGRES_DB"
              value = "gtfs_scheduled"
            },
            {
              name = "POSTGRES_USER"
              value = "testuser"
            },
            {
              name = "POSTGRES_PASSWORD"
              value = "testpass"
            }
          ]
        }
      }
    }
  }
}

resource "kubernetes_service" "bzn_scheduled_db_service" {
  metadata {
    name = "bzn-scheduled-db"
  }
  spec {
    selector {
      test = "${kubernetes_deployment.bzn_scheduled_db_deployment.metadata.0.labels.test}"
    }
    port {
      port = 5432 
      target_port = 5432 
    }
  }
}

resource "kubernetes_deployment" "bzn_scheduled_api_deployment" {
  metadata {
    name = "terraform-bzn-scheduled-api-deployment"
    labels {
      test = "bzn-scheduled-api"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels {
        test = "bzn-scheduled-api"
      }
    }

    template {
      metadata {
        labels {
          test = "bzn-scheduled-api"
        }
      }

      spec {
        image_pull_secrets {
          name = "docker-cfg" 
        }
        container {
          image = "${var.img_scheduled_api}"
          name  = "bzn-scheduled-api"
          port {
            container_port = 8080
          }
          env   = [
            {
              name = "SPRING_PROFILES_ACTIVE"
              value = "PRODUCTION"
            },
            {
              name = "BZD_PSQL_HOSTNAME"
              value = "bzn-scheduled-db"
            },
            {
              name = "BZD_PSQL_PORT"
              value = "5432"
            },
            {
              name = "BZD_PSQL_DB"
              value = "gtfs_scheduled"
            },
            {
              name = "BZD_PSQL_USER"
              value = "testuser"
            },
            {
              name = "BZD_PSQL_PASS"
              value = "testpass"
            }
          ]
        }
      }
    }
  }
}
resource "kubernetes_service" "bzn_scheduled_api_service" {
  metadata {
    name = "bzn-scheduled-api"
  }
  spec {
    selector {
      test = "${kubernetes_deployment.bzn_scheduled_api_deployment.metadata.0.labels.test}"
    }
    port {
      port = 8080
      target_port = 8080
    }

    type = "LoadBalancer"
  }
}



# OUTPUTS ---------------------------------------------------------------------
output "host" {
  value = "https://${var.host}"
}
output "lb_ip" {
  value = "${kubernetes_service.bzn_scheduled_api_service.load_balancer_ingress.0.ip}"
}
